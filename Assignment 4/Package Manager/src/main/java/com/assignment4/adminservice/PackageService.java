package com.assignment4.adminservice;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.DOCUMENT)
public interface PackageService {

    @WebMethod
    String findAllPackages();

    @WebMethod
    void saveRoute();

    @WebMethod
    void savePackage();

    @WebMethod
    String findAllRoutes();
}