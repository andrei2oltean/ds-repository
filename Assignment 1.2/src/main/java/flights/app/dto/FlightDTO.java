package flights.app.dto;

import java.io.Serializable;
import java.sql.Date;

/**
 * Created by Andrei on 01/11/2016.
 */
public class FlightDTO implements Serializable {

    private Integer flightNumber;
    private String airplaneType;
    private String departureCityName;
    private Double departureCityLongitude;
    private Double departureCityLatitude;
    private Date departureTime;
    private String arrivalCityName;
    private Double arrivalCityLongitude;
    private Double arrivalCityLatitude;
    private Date arrivalTime;

    public FlightDTO() {
    }

    public FlightDTO(Integer flightNumber, String airplaneType, String departureCityName, Double departureCityLongitude, Double departureCityLatitude, Date departureTime, String arrivalCityName, Double arrivalCityLongitude, Double arrivalCityLatitude, Date arrivalTime) {
        this.flightNumber = flightNumber;
        this.airplaneType = airplaneType;
        this.departureCityName = departureCityName;
        this.departureCityLongitude = departureCityLongitude;
        this.departureCityLatitude = departureCityLatitude;
        this.departureTime = departureTime;
        this.arrivalCityName = arrivalCityName;
        this.arrivalCityLongitude = arrivalCityLongitude;
        this.arrivalCityLatitude = arrivalCityLatitude;
        this.arrivalTime = arrivalTime;
    }

    public Integer getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(Integer flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public String getDepartureCityName() {
        return departureCityName;
    }

    public void setDepartureCityName(String departureCityName) {
        this.departureCityName = departureCityName;
    }

    public Double getDepartureCityLongitude() {
        return departureCityLongitude;
    }

    public void setDepartureCityLongitude(Double departureCityLongitude) {
        this.departureCityLongitude = departureCityLongitude;
    }

    public Double getDepartureCityLatitude() {
        return departureCityLatitude;
    }

    public void setDepartureCityLatitude(Double departureCityLatitude) {
        this.departureCityLatitude = departureCityLatitude;
    }

    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalCityName() {
        return arrivalCityName;
    }

    public void setArrivalCityName(String arrivalCityName) {
        this.arrivalCityName = arrivalCityName;
    }

    public Double getArrivalCityLongitude() {
        return arrivalCityLongitude;
    }

    public void setArrivalCityLongitude(Double arrivalCityLongitude) {
        this.arrivalCityLongitude = arrivalCityLongitude;
    }

    public Double getArrivalCityLatitude() {
        return arrivalCityLatitude;
    }

    public void setArrivalCityLatitude(Double arrivalCityLatitude) {
        this.arrivalCityLatitude = arrivalCityLatitude;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public static class Builder {
        private Integer nestedFlightNumber;
        private String nestedAirplaneType;
        private String nestedDepartureCityName;
        private Double nestedDepartureCityLongitude;
        private Double nestedDepartureCityLatitude;
        private Date nestedDepartureTime;
        private String nestedArrivalCityName;
        private Double nestedArrivalCityLongitude;
        private Double nestedArrivalCityLatitude;
        private Date nestedArrivalTime;

        public Builder flightNumber(Integer flightNumber) {
            this.nestedFlightNumber = flightNumber;
            return this;
        }

        public Builder airplaneType(String airplaneType) {
            this.nestedAirplaneType = airplaneType;
            return this;
        }

        public Builder departureCityName(String departureCityName) {
            this.nestedDepartureCityName = departureCityName;
            return this;
        }

        public Builder departureCityLongitude(Double departureCityLongitude) {
            this.nestedDepartureCityLongitude = departureCityLongitude;
            return this;
        }

        public Builder departureCityLatitude(Double departureCityLatitude) {
            this.nestedDepartureCityLatitude = departureCityLatitude;
            return this;
        }

        public Builder departureTime(Date departureTime) {
            this.nestedDepartureTime = departureTime;
            return this;
        }

        public Builder arrivalCityName(String arrivalCityName) {
            this.nestedArrivalCityName = arrivalCityName;
            return this;
        }

        public Builder arrivalCityLongitude(Double arrivalCityLongitude) {
            this.nestedArrivalCityLongitude = arrivalCityLongitude;
            return this;
        }

        public Builder arrivalCityLatitude(Double arrivalCityLatitude) {
            this.nestedArrivalCityLatitude = arrivalCityLatitude;
            return this;
        }

        public Builder arrivalTime(Date arrivalTime) {
            this.nestedArrivalTime = arrivalTime;
            return this;
        }

        public FlightDTO create() {
            return new FlightDTO(nestedFlightNumber, nestedAirplaneType, nestedDepartureCityName, nestedDepartureCityLongitude, nestedDepartureCityLatitude, nestedDepartureTime, nestedArrivalCityName, nestedArrivalCityLongitude, nestedArrivalCityLatitude, nestedArrivalTime);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FlightDTO flightDTO = (FlightDTO) o;

        if (flightNumber != null ? !flightNumber.equals(flightDTO.flightNumber) : flightDTO.flightNumber != null)
            return false;
        if (airplaneType != null ? !airplaneType.equals(flightDTO.airplaneType) : flightDTO.airplaneType != null)
            return false;
        if (departureCityName != null ? !departureCityName.equals(flightDTO.departureCityName) : flightDTO.departureCityName != null)
            return false;
        if (departureCityLongitude != null ? !departureCityLongitude.equals(flightDTO.departureCityLongitude) : flightDTO.departureCityLongitude != null)
            return false;
        if (departureCityLatitude != null ? !departureCityLatitude.equals(flightDTO.departureCityLatitude) : flightDTO.departureCityLatitude != null)
            return false;
        if (departureTime != null ? !departureTime.equals(flightDTO.departureTime) : flightDTO.departureTime != null)
            return false;
        if (arrivalCityName != null ? !arrivalCityName.equals(flightDTO.arrivalCityName) : flightDTO.arrivalCityName != null)
            return false;
        if (arrivalCityLongitude != null ? !arrivalCityLongitude.equals(flightDTO.arrivalCityLongitude) : flightDTO.arrivalCityLongitude != null)
            return false;
        if (arrivalCityLatitude != null ? !arrivalCityLatitude.equals(flightDTO.arrivalCityLatitude) : flightDTO.arrivalCityLatitude != null)
            return false;
        return arrivalTime != null ? arrivalTime.equals(flightDTO.arrivalTime) : flightDTO.arrivalTime == null;

    }

    @Override
    public int hashCode() {
        int result = flightNumber != null ? flightNumber.hashCode() : 0;
        result = 31 * result + (airplaneType != null ? airplaneType.hashCode() : 0);
        result = 31 * result + (departureCityName != null ? departureCityName.hashCode() : 0);
        result = 31 * result + (departureCityLongitude != null ? departureCityLongitude.hashCode() : 0);
        result = 31 * result + (departureCityLatitude != null ? departureCityLatitude.hashCode() : 0);
        result = 31 * result + (departureTime != null ? departureTime.hashCode() : 0);
        result = 31 * result + (arrivalCityName != null ? arrivalCityName.hashCode() : 0);
        result = 31 * result + (arrivalCityLongitude != null ? arrivalCityLongitude.hashCode() : 0);
        result = 31 * result + (arrivalCityLatitude != null ? arrivalCityLatitude.hashCode() : 0);
        result = 31 * result + (arrivalTime != null ? arrivalTime.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FlightDTO{" +
                "flightNumber=" + flightNumber +
                ", airplaneType='" + airplaneType + '\'' +
                ", departureCityName='" + departureCityName + '\'' +
                ", departureCityLongitude=" + departureCityLongitude +
                ", departureCityLatitude=" + departureCityLatitude +
                ", departureTime=" + departureTime +
                ", arrivalCityName='" + arrivalCityName + '\'' +
                ", arrivalCityLongitude=" + arrivalCityLongitude +
                ", arrivalCityLatitude=" + arrivalCityLatitude +
                ", arrivalTime=" + arrivalTime +
                '}';
    }
}
