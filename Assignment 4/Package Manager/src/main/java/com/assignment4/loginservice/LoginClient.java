package com.assignment4.loginservice;

import com.assignment4.entities.App_user;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class LoginClient {

    private static final String WS_URL = "http://localhost:9999/ws/hello?wsdl";

    public static App_user useLogInService(String username, String password) {

        ClientService clientService = putUserNamePassword(username, password);
        if (clientService == null) return null;

        return clientService.getLogIn();
    }

    public static App_user useRegisterService(String username, String password) {
        ClientService clientService = putUserNamePassword(username, password);
        if (clientService == null) return null;
        return clientService.getRegister();
    }

    private static ClientService putUserNamePassword(String username, String password) {
        URL url = null;
        try {
            url = new URL(WS_URL);
        } catch (MalformedURLException e) {
            return null;
        }
        QName qname = new QName("http://publisher/", "ClientServiceImplService");

        Service service = Service.create(url, qname);
        ClientService hello = null; //getPort(ClientService.class);
        Iterator<QName> it = service.getPorts();
        while (it.hasNext()) {
            QName port = it.next();
            if("ClientServiceImplPort".equals(port.getLocalPart())) {
                hello = service.getPort(port, ClientService.class);
            }
        }

        /*******************UserName & Password ******************************/
        Map<String, Object> req_ctx = ((BindingProvider) hello).getRequestContext();
        req_ctx.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, WS_URL);

        Map<String, List<String>> headers = new HashMap<String, List<String>>();
        headers.put("Username", Collections.singletonList(username));
        headers.put("Password", Collections.singletonList(password));
        req_ctx.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        /**********************************************************************/return hello;
    }
}
