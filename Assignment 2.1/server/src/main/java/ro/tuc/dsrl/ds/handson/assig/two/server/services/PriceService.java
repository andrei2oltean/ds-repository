package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.IPriceService;

/**
 * Created by Andrei on 09/11/2016.
 */
public class PriceService implements IPriceService {

    @Override
    public double calculateSellingPrice(Car c) {
        if (c.getPurchasingPrice() < 0) {
            throw new IllegalArgumentException("Car purchasing price must be positive");
        }
        return c.getPurchasingPrice() - c.getPurchasingPrice() / 7 * (2015 - c.getYear());
    }
}
