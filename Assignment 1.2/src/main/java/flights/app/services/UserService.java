package flights.app.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import flights.app.dto.UserDTO;
import flights.app.entities.User;
import flights.app.errorhandler.EntityValidationException;
import flights.app.errorhandler.ResourceNotFoundException;
import flights.app.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	@Autowired
	private UserRepository usrRepository;

	public UserDTO findUserById(int userId) {
		User usr = usrRepository.findById(userId);
		if (usr == null) {
			throw new ResourceNotFoundException(User.class.getSimpleName());
		}

		UserDTO dto = new UserDTO.Builder()
						.id(usr.getId())
						.username(usr.getUsername())
						.password(usr.getPassword())
						.email(usr.getEmail())
						.create();
		return dto;
	}
	
	public List<UserDTO> findAll() {
		List<User> users = usrRepository.findAll();
		List<UserDTO> toReturn = new ArrayList<UserDTO>();
		for (User user : users) {
			UserDTO dto = new UserDTO.Builder()
					.id(user.getId())
					.username(user.getUsername())
					.password(user.getPassword())
					.email(user.getEmail())
					.create();
			toReturn.add(dto);
		}
		return toReturn;
	}

	public int create(UserDTO userDTO) {
		List<String> validationErrors = validateUser(userDTO);
		if (!validationErrors.isEmpty()) {
			throw new EntityValidationException(User.class.getSimpleName(),validationErrors);
		}

		User user = new User();
		user.setUsername(userDTO.getUsername());
		user.setEmail(userDTO.getEmail());
		user.setPassword(userDTO.getPassword());
		user.setCreated(new Date());

		User usr = usrRepository.save(user);
		return usr.getId();
	}

	private List<String> validateUser(UserDTO usr) {
		List<String> validationErrors = new ArrayList<String>();

		if (usr.getUsername() == null || "".equals(usr.getUsername())) {
			validationErrors.add("Name field should not be empty");
		}

		if (usr.getEmail() == null || !validateEmail(usr.getEmail())) {
			validationErrors.add("Email does not have the correct format.");
		}

		if (usr.getPassword() == null || "".equals(usr.getPassword())) {
			validationErrors.add("Password field should not be empty");
		}

		return validationErrors;
	}

	public static boolean validateEmail(String email) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
		return matcher.find();
	}
}
