package flights.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import flights.app.entities.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	User findByUsername(String username);

	User findById(int id);

}
