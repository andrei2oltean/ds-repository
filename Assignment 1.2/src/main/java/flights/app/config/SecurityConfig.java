package flights.app.config;

import flights.app.controller.AuthenticationSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("andrei@noemail.com").password("123456").roles("USER");
        auth.inMemoryAuthentication().withUser("admin@flightsapp.com").password("123456").roles("ADMIN");
        auth.inMemoryAuthentication().withUser("dba@flightsapp.com").password("123456").roles("DBA");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
            .csrf().disable()
            .authorizeRequests()
                .antMatchers("/flights").authenticated()
                .antMatchers("/admin").access("hasRole('ROLE_ADMIN')")
                .antMatchers("/user").access("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
                .antMatchers("/dba").access("hasRole('ROLE_ADMIN')or hasRole('ROLE_DBA')")
            .and()
                .formLogin().loginPage("/login")
                .usernameParameter("username").passwordParameter("password")
                .permitAll().successHandler(new AuthenticationSuccessHandler());
    }
}
