package com.assignment4.serviceclients;

import com.assignment4.entities.RouteEntry;
import wsjavaclient.ClientPackageServiceImpl;
import wsjavaclient.ClientPackageServiceImplService;
import wsjavaclient.Package;
import wsjavaclient.User;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClientService {

    ClientPackageServiceImpl clientService;

    public ClientService() {
        ClientPackageServiceImplService clientPackageService = new ClientPackageServiceImplService();
        clientService = clientPackageService.getClientPackageServiceImplPort();
    }

    public User logIn(String username, String password) {
        return clientService.getLogIn(username, password);
    }

    public User register(String username, String password) {
        return clientService.getRegister(username, password);
    }

    public List<Package> getPackages() {
        return clientService.getAllPackages();
    }

    public List<RouteEntry> getRoutes(int packageId) {
        return getAllRoutes(packageId);
    }

    private List<RouteEntry> getAllRoutes(int packageId) {
        List<RouteEntry> converted = new ArrayList<>();
        List<wsjavaclient.RouteEntry> allRoutes = clientService.getAllRoutes(packageId);
        for (wsjavaclient.RouteEntry entry : allRoutes) {
            RouteEntry routeEntry = new RouteEntry();
            routeEntry.setCity(entry.getCity());
            XMLGregorianCalendar time = entry.getTime();
            routeEntry.setTime(toDate(time));
            converted.add(routeEntry);
        }
        return converted;
    }

    public static Date toDate(XMLGregorianCalendar calendar) {
        if (calendar == null) {
            return null;
        }
        return calendar.toGregorianCalendar().getTime();
    }
}
