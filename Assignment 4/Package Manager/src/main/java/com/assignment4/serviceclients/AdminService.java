package com.assignment4.serviceclients;

import wsjavaclient.Package;

import java.util.Date;

public abstract class AdminService {

    public static AdminService getService(String serviceType) {
        return new AdminJavaService();
    }

    abstract public void addPackage(Package packageEntity);

    abstract public void removePackage(int id);

    abstract public void registerForTracking(int packageId);

    abstract public void addRoute(String city, Date date, int packageId);
}
