package service;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class DBUtils {

    private static Connection conn = getConnection();

    public static Connection getConnection() {
        if (conn != null)
            return conn;

        InputStream inputStream = DBUtils.class.getClassLoader().getResourceAsStream("db.properties");
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
            String driver = properties.getProperty("driver");
            String url = properties.getProperty("url");
            String user = properties.getProperty("user");
            String password = properties.getProperty("password");
            Class.forName(driver);
            conn = DriverManager.getConnection(url, user, password);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return conn;
    }

    public static void closeConnection(Connection toBeClosed) {
        if (toBeClosed == null)
            return;
        try {
            toBeClosed.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User register(String username, String password) {
        try {
            String query = "insert into users " +
                    "(username, password, role) " +
                    "values (?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            preparedStatement.setString(3, "user");
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return logIn(username, password);
    }

    public User logIn(String username, String password) {
        User user = new User();
        try {
            String query = "select * from users where username=? and password=?";
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
                user.setUsername(username);
                user.setPassword(password);
                user.setRole(resultSet.getString("role"));
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public List<Package> allPackages() {
        List<Package> allPackages = new ArrayList<Package>();
        try {
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from packages");
            while (resultSet.next()) {
                Package aPackage = new Package();
                aPackage.setId(resultSet.getInt("id"));
                aPackage.setSender(resultSet.getInt("sender_id"));
                aPackage.setReceiver(resultSet.getInt("receiver_id"));
                aPackage.setName(resultSet.getString("name"));
                aPackage.setReceiverCity(resultSet.getString("receiver_city"));
                aPackage.setDescription(resultSet.getString("description"));
                aPackage.setSenderCity(resultSet.getString("sender_city"));
                aPackage.setTracking(resultSet.getBoolean("tracking"));
                allPackages.add(aPackage);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allPackages;
    }

    public List<RouteEntry> getAllRoutes(int packageId) {
        List<RouteEntry> routeEntries = new ArrayList<>();
        try {
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from routes " +
                    "where package_id=" + packageId);
            while (resultSet.next()) {
                RouteEntry routeEntry = new RouteEntry();
                routeEntry.setId(resultSet.getInt("id"));
                routeEntry.setCity(resultSet.getString("city"));
                routeEntry.setTime(resultSet.getDate("time"));
                routeEntries.add(routeEntry);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return routeEntries;
    }
}
