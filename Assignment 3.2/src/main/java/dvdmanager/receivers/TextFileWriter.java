package dvdmanager.receivers;

import dvdmanager.message.MessageContent;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TextFileWriter {

    private static final String MSG_PATH = (new File(".")).getAbsolutePath().replace(".", "messages\\");
    private static final String MSG_TYPE = ".txt";

    protected static void log(MessageContent message) {
        int messageIndex = 0;
        String fileName = MSG_PATH + "Message " + messageIndex + MSG_TYPE;
        File messageFile = new File(fileName);
        // search for unique file name in path
        while (messageFile.exists()) {
            messageIndex++;
            fileName = MSG_PATH + "Message " + messageIndex + MSG_TYPE;
            messageFile = new File(fileName);
        }
        try {
            if (!messageFile.createNewFile()) {
                System.out.println("File exists!");
                return;
            }

            BufferedWriter writer = new BufferedWriter(new FileWriter(messageFile));
            writer.write(message.toString());
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
