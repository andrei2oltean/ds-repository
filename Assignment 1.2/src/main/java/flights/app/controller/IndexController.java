package flights.app.controller;

import flights.app.dto.FlightDTO;
import flights.app.errorhandler.FlightValidator;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

@CrossOrigin(maxAge = 3600)
@Controller
public class IndexController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String start() {
        return "welcome";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView welcomeScreen(@RequestParam(value = "error", required = false) String error,
                                @RequestParam(value = "logout", required = false) String logout) {
        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid username and password!");
        }
        if (logout != null) {
            model.addObject("msg", "You've been logged out successfully.");
        }

        model.setViewName("login");

        return model;
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }

    @RequestMapping(value = "/flights", method = RequestMethod.GET)
    public String didUserLogin() {
        return "flights";
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String redirectUser(ModelMap model) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<FlightDTO[]> responseEntity = restTemplate.getForEntity("http://localhost:8080/flights/all", FlightDTO[].class);
        FlightDTO[] flights = responseEntity.getBody();
        model.addAttribute("flights", flights);

        return "user";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String redirectAdmin(ModelMap model) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<FlightDTO[]> responseEntity = restTemplate.getForEntity("http://localhost:8080/flights/all", FlightDTO[].class);
        FlightDTO[] flights = responseEntity.getBody();
        model.addAttribute("flights", flights);

        return "admin";
    }

    @RequestMapping(value = "/admin/crud", method = RequestMethod.GET)
    public String adminCRUD(@Valid FlightDTO flight, BindingResult result, ModelMap model, @RequestParam String action) {

        switch (action) {
            case "Insert flight": {
                String uri = "http://localhost:8080/flights/insert";
                RestTemplate restTemplate = new RestTemplate();
                Integer flightId = restTemplate.postForObject(uri, flight, Integer.class);
                break;
            }
            case "Update flight": {
                String uri = "http://localhost:8080/flights/update";
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.put(uri, flight);
                break;
            }
            case "Delete flight": {
                String uri = "http://localhost:8080/flights/delete/" + flight.getFlightNumber();
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.delete(uri);
                break;
            }
            default:
                break;
        }

        return "redirect:/admin";
    }

    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.addValidators(new FlightValidator());
    }
}
