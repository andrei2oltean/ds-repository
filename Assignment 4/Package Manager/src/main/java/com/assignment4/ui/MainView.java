package com.assignment4.ui;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

/** Main view with a menu (with declarative layout design) */
@DesignRoot
public class MainView extends HorizontalLayout {
    public static final String NAME = "main";

    Navigator navigator;

    class AnimalButton extends Button {
        public AnimalButton(String caption, String id) {
            super(caption);
            addStyleName(ValoTheme.MENU_ITEM);
            setIcon(new ThemeResource("img/" + id + "-16px.png"));
            addClickListener(click -> navigator.navigateTo(id));
        }
    }

    Label title;
    CssLayout menu;
    Panel contentArea;
    Button logoutButton;

    public MainView(String username, Runnable logout, String titleName) {
        setSizeFull();
        addStyleName(ValoTheme.MENU_ROOT);

        title = new Label(titleName);
        title.addStyleName(ValoTheme.MENU_TITLE);

        menu = new CssLayout();
        menu.addStyleName(ValoTheme.MENU_PART);
        addComponent(menu);

        menu.addComponent(new AnimalButton("Pig",      "pig"));
        menu.addComponent(new AnimalButton("Cat",      "cat"));
        menu.addComponent(new AnimalButton("Dog",      "dog"));
        menu.addComponent(new AnimalButton("Reindeer", "reindeer"));
        menu.addComponent(new AnimalButton("Penguin",  "penguin"));
        menu.addComponent(new AnimalButton("Sheep",    "sheep"));

        contentArea = new Panel("Equals");
        contentArea.addStyleName(ValoTheme.PANEL_BORDERLESS);
        contentArea.setSizeFull();
        addComponent(contentArea);
        setExpandRatio(contentArea, 1.0f);

        // Create a navigator to control the sub-views
        navigator = new Navigator(UI.getCurrent(), contentArea);

        // Create and register the sub-views
        navigator.addView("pig", AnimalView.class);
        navigator.addView("cat", AnimalView.class);
        navigator.addView("dog", AnimalView.class);
        navigator.addView("reindeer", AnimalView.class);
        navigator.addView("penguin",  AnimalView.class);
        navigator.addView("sheep",    AnimalView.class);

        // Allow going back to the start
        logoutButton = new Button("Log Out");
        logoutButton.addClickListener(click -> logout.run());
        addComponent(logoutButton);
    }

        /*
        @Override
        public void enter(ViewChangeEvent event) {
            if (event.getParameters() == null
                || event.getParameters().isEmpty()) {
                equalPanel.setContent(
                    new Label("Nothing to see here, " +
                              "just pass along."));
                return;
            } else
                equalPanel.setContent(new AnimalViewer(
                    event.getParameters()));
        }*/
}
