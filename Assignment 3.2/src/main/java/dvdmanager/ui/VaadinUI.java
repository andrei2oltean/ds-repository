package dvdmanager.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import dvdmanager.entities.DVDEntity;
import dvdmanager.repository.DVDRepository;
import org.springframework.beans.factory.annotation.Autowired;

@SpringUI
@Theme("valo")
public class VaadinUI extends UI {

    private final DVDRepository dvdRepository;
    private final DVDEditor dvdEditor;

    private final Grid dvdGrid;

    @Autowired
    public VaadinUI(DVDRepository dvdRepository, DVDEditor dvdEditor) {
        this.dvdEditor = dvdEditor;
        this.dvdRepository = dvdRepository;
        this.dvdGrid = new Grid();
    }

    @Override
    protected void init(VaadinRequest request) {
        // build layout
        dvdEditor.setVisible(true);
        VerticalLayout dvdMainLayout = new VerticalLayout(dvdEditor, dvdGrid);
        dvdMainLayout.setMargin(true);
        dvdMainLayout.setSpacing(true);

        setContent(new HorizontalLayout(dvdMainLayout));

        // Configure layouts and components
        dvdGrid.setHeight(300, Unit.PIXELS);
        dvdGrid.setColumns("id", "title", "year", "price");

        initActions();

        // Initialize listing
        listDvds();

    }
    private void initActions() {
        initDvdActions();
    }

    private void initDvdActions() {
        dvdGrid.addSelectionListener(e -> {
            dvdEditor.editDVD((DVDEntity) dvdGrid.getSelectedRow());
        });

        // Listen changes made by the dvdEditor, refresh data from backend
        dvdEditor.setChangeHandler(() -> {
            listDvds();
        });
    }

    @SuppressWarnings("unchecked")
    private void listDvds() {
        dvdGrid.setContainerDataSource(new BeanItemContainer(DVDEntity.class, dvdRepository.findAll()));
    }
}


