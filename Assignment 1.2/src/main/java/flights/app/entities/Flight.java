package flights.app.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/**
 * Created by Andrei on 30/10/2016.
 */
@Entity
@Table(name = "flight")
public class Flight implements Serializable {

    private Integer flightNumber;
    private String airplaneType;
    private String departureCityId;
    private Date departureTime;
    private String arrivalCityId;
    private Date arrivalTime;

    public Flight() {
    }

    public Flight(Integer flightNumber, String airplaneType, String departureCityId, Date departureTime, String arrivalCityId, Date arrivalTime) {
        this.flightNumber = flightNumber;
        this.airplaneType = airplaneType;
        this.departureCityId = departureCityId;
        this.departureTime = departureTime;
        this.arrivalCityId = arrivalCityId;
        this.arrivalTime = arrivalTime;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "flight_number", unique = true, nullable = false)
    public Integer getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(Integer flightNumber) {
        this.flightNumber = flightNumber;
    }

    @Column(name = "airplane_type")
    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    @Column(name = "departure_city_id", nullable = false)
    public String getDepartureCityId() {
        return departureCityId;
    }

    public void setDepartureCityId(String departureCityId) {
        this.departureCityId = departureCityId;
    }

    @Column(name = "departure_time", nullable = false)
    public Date getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    @Column(name = "arrival_city_id", nullable = false)
    public String getArrivalCityId() {
        return arrivalCityId;
    }

    public void setArrivalCityId(String arrivalCityId) {
        this.arrivalCityId = arrivalCityId;
    }

    @Column(name = "arrival_time", nullable = false)
    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
}
