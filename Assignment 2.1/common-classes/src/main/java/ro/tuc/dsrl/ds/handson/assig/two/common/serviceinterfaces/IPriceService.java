package ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;

/**
 * Created by Andrei on 09/11/2016.
 */
public interface IPriceService {

    /**
     * Computes the selling price of a car
     * @param c car for which to compute the selling price
     * @return selling price of the car
     */
    double calculateSellingPrice(Car c);
}
