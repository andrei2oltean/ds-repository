package service;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Date;

@WebService
public interface AdminPackageService {

    @WebMethod
    public void addPackage(Package packageEntity);

    @WebMethod
    public void removePackage(int id);

    @WebMethod
    public void registerForTracking(int packageId);

    @WebMethod
    public void addRoute(String city, Date time, int packageId);
}


