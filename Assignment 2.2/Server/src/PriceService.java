import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Andrei on 02/12/2016.
 */
public class PriceService extends UnicastRemoteObject implements IPriceService {
    public PriceService() throws RemoteException {
        super();
    }

    @Override
    public double computeSellingPrice(double purchasingPrice, int year) throws RemoteException {
        if (purchasingPrice < 0) {
            throw new IllegalArgumentException("Car purchasing price must be positive");
        }
        return purchasingPrice - purchasingPrice / 7 * (2015 - year);
    }
}
