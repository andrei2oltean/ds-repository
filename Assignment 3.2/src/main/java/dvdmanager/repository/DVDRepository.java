package dvdmanager.repository;

import dvdmanager.entities.DVDEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DVDRepository extends JpaRepository<DVDEntity, Long> {

}
