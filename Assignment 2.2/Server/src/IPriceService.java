import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Andrei on 02/12/2016.
 */
public interface IPriceService extends Remote {
    double computeSellingPrice(final double purchasingPrice, final int year) throws RemoteException;
}
