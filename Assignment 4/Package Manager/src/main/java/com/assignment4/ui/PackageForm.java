package com.assignment4.ui;

import com.assignment4.serviceclients.AdminService;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.*;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.ValoTheme;
import wsjavaclient.Package;

/* Create custom UI Components.
 *
 * Create your own Vaadin components by inheritance and composition.
 * This is a form component inherited from VerticalLayout. Use
 * Use BeanFieldGroup to bind data fields from DTO to UI fields.
 * Similarly named field by naming convention or customized
 * with @PropertyId annotation.
 */
public class PackageForm extends FormLayout {

    private AdminService adminService = AdminService.getService("java");

    Button save = new Button("Save", this::save);
    Button cancel = new Button("Cancel", this::cancel);
    TextField sender = new TextField("Sender email");
    TextField senderCity = new TextField("Sender city");
    TextField receiverCity = new TextField("Receiver city");
    TextField receiver = new TextField("Receiver email");
    TextField name = new TextField("Package name");
    TextField description = new TextField("Package description");
    CheckBox tracking = new CheckBox("Tracking", false);

    AdminView view;
    Package selectedPackageObj;

    BeanFieldGroup<Package> formFieldBindings;

    public PackageForm(AdminView view) {
        this.view = view;
        configureComponents();
        buildLayout();
    }

    private void configureComponents() {
        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        setVisible(false);
    }

    private void buildLayout() {
        setSizeUndefined();
        setMargin(true);

        HorizontalLayout actions = new HorizontalLayout(save, cancel);
        actions.setSpacing(true);

        addComponents(actions, sender, senderCity, receiver, receiverCity, name, description, tracking);
    }

    public void save(Button.ClickEvent event) {
        try {
            formFieldBindings.commit();

            adminService.addPackage(selectedPackageObj);

            String msg = String.format("Saved '%s %s'.", selectedPackageObj.getReceiver(), selectedPackageObj.getSender());
            Notification.show(msg, Type.TRAY_NOTIFICATION);
            view.refreshPackages();
        } catch (FieldGroup.CommitException e) {
            // Validation exceptions could be shown here
        }
    }

    public void cancel(Button.ClickEvent event) {
        // Place to call business logic.
        Notification.show("Cancelled", Type.TRAY_NOTIFICATION);
        view.packageList.select(null);
    }

    void edit(Package packageObj) {
        this.selectedPackageObj = packageObj;
        if (packageObj != null) {
            // Bind the properties of the selectedPackageObj POJO to fiels in this form
            formFieldBindings = BeanFieldGroup.bindFieldsBuffered(packageObj, this);
            name.focus();
        }
        setVisible(packageObj != null);
    }

}