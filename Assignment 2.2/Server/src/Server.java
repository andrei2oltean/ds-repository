import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by Andrei on 27/11/2016.
 */
public class Server {

    private void startServer() {
        try {
            // create on port 1097
            Registry registry = LocateRegistry.createRegistry(1097);
            registry.rebind("computeTax", new TaxService());
            registry.rebind("computeSellingPrice", new PriceService());
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Server has started");
    }

    public static void main(String[] args) {
        Server server = new Server();
        server.startServer();
    }
}
