package flights.app.tests.integration;


import static org.junit.Assert.assertTrue;

import java.util.List;

import flights.app.dto.UserDTO;
import flights.app.errorhandler.EntityValidationException;
import flights.app.tests.config.TestJPAConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import flights.app.errorhandler.ResourceNotFoundException;
import flights.app.services.UserService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestJPAConfiguration.class})
@Transactional
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void testCreate() {

        UserDTO dto1 = new UserDTO.Builder()
                .username("User")
                .password("unprotected")
                .email("user@gmail.com")
                .create();

        UserDTO dto2 = new UserDTO.Builder()
                .username("User2")
                .password("123456")
                .email("user2@gmail.com")
                .create();

        userService.create(dto1);
        userService.create(dto2);

        List<UserDTO> fromDB = userService.findAll();

        assertTrue("One entity inserted", fromDB.size() == 2);
    }

    @Test
    public void testGetByIdSuccessful() {

        UserDTO dto = new UserDTO.Builder()
                .username("User")
                .password("empty")
                .email("user@gmail.com")
                .create();

        int userId = userService.create(dto);
        UserDTO fromDB = userService.findUserById(userId);

        assertTrue("Name ", dto.getUsername().equals(fromDB.getUsername()));
        assertTrue("Password", dto.getPassword().equals(fromDB.getPassword()));
        assertTrue("Email ", dto.getEmail().equals(fromDB.getEmail()));
    }

    @Test
    public void testGetByIdDoubleFirstName() {

        UserDTO dto = new UserDTO.Builder()
                .username("Another User")
                .email("trilulilu@gmail.com")
                .password("youshallnotpass")
                .create();

        int userId = userService.create(dto);
        UserDTO fromDB = userService.findUserById(userId);

        assertTrue("Name ", dto.getUsername().equals(fromDB.getUsername()));
    }

    @Test(expected = EntityValidationException.class)
    public void testCreateUnsuccessfulEmail() {

        UserDTO dto = new UserDTO.Builder()
                .username("First")
                .email("notanemail")
                .password("letmein")
                .create();

        int userId = userService.create(dto);
        UserDTO fromDB = userService.findUserById(userId);

        assertTrue("Name ", dto.getUsername().equals(fromDB.getUsername()));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testGetByIdUnsuccessful() {

        UserDTO dto = new UserDTO.Builder()
                .username("Me")
                .password("000000")
                .email("me@yahoo.com")
                .create();

        int userId = userService.create(dto);
        userService.findUserById(userId + 1);
    }

}
