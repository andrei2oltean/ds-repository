package com.assignment4.ui;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

@DesignRoot
public class AnimalView extends VerticalLayout implements View {
    Label watching;
    Embedded pic;
    Label back;

    public AnimalView() {
        Design.read(this);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        String animal = event.getViewName();

        watching.setValue("You are currently watching a " +
                animal);
        pic.setSource(new ThemeResource(
                "img/" + animal + "-128px.png"));
        back.setValue("and " + animal +
                " is watching you back");
    }
}