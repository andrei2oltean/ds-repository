package flights.app.repositories;

import flights.app.entities.City;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Andrei on 30/10/2016.
 */
public interface CityRepository extends JpaRepository<City, Integer> {

    City findByName(String name);

    City findById(Integer id);
}
