package dvdmanager.ui;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;
import dvdmanager.entities.DVDEntity;
import dvdmanager.queue.QueueRunner;
import dvdmanager.repository.DVDRepository;
import org.springframework.beans.factory.annotation.Autowired;

@SpringComponent
@UIScope
public class DVDEditor extends VerticalLayout {

	private final DVDRepository repository;

	private DVDEntity dvd;

	/* Fields to edit properties in Dvd entity */
	private TextField title = new TextField("Title");
	private TextField year = new TextField("Year");
	private TextField price = new TextField("Price");

	/* Action buttons */
	private Button save = new Button("Save", FontAwesome.SAVE);
	private Button cancel = new Button("Cancel");

	private CssLayout actions = new CssLayout(save, cancel);

	@Autowired
	public DVDEditor(DVDRepository repository) {
		this.repository = repository;

		addComponents(title, year, price, actions);

		// Configure and style components
		setSpacing(true);
		actions.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
		save.setStyleName(ValoTheme.BUTTON_PRIMARY);
		save.setClickShortcut(ShortcutAction.KeyCode.ENTER);

		// wire action buttons to save and reset(cancel)
		save.addClickListener(e -> addNewDvd());
		cancel.addClickListener(e -> clearTextFields());
		setVisible(true);
	}

	private void clearTextFields() {
		title.clear();
		year.clear();
		price.clear();
	}

	private DVDEntity addNewDvd() {
		DVDEntity dvd = new DVDEntity(title.getValue(), Integer.parseInt(year.getValue()), Double.parseDouble(price.getValue()));
		QueueRunner.sendMessage(dvd);
		return repository.save(dvd);
	}

	public interface ChangeHandler {
		void onChange();
	}

	public final void editDVD(DVDEntity d) {
		final boolean persisted = d.getId() != null;
		if (persisted) {
			// Find fresh entity for editing
			dvd = repository.findOne(d.getId());
		}
		else {
			dvd = d;
		}

		// Bind dvd properties to similarly named fields
		// Could also use annotation or "manual binding" or programmatically
		// moving values from fields to entities before saving
		BeanFieldGroup.bindFieldsUnbuffered(dvd, this);

		// A hack to ensure the whole form is visible
		save.focus();
		// Select all text in name field automatically
		title.selectAll();
	}

	public void setChangeHandler(ChangeHandler h) {
		// ChangeHandler is notified when either save is clicked
		save.addClickListener(e -> h.onChange());
	}

}
