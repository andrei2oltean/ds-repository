package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;

import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.service.MailService;
import ro.tuc.dsrl.ds.handson.assig.three.queue.communication.DVD;
import ro.tuc.dsrl.ds.handson.assig.three.queue.communication.MessageContent;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Consumer Client application. This application
 *  will run in an infinite loop and retrieve messages from the queue server
 *  and send e-mails with them as they come.
 */
public class ClientStartTexts {

	private ClientStartTexts() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection("localhost",8888);

		DVD message;

		while(true) {
			try {
				message = (DVD) queue.readMessage();
				BufferedWriter writer;
				File textFile = new File(message.getTitle() + " " + Integer.toString(message.getYear()));
				writer = new BufferedWriter(new FileWriter(textFile));
				writer.write(message.toString());
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
