package service;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ClientPackageServiceImpl implements ClientPackageService {
    private DBUtils db;

    public ClientPackageServiceImpl() {
        db = new DBUtils();
    }

    @WebMethod
    public User getLogIn(String username, String password) {
        return db.logIn(username, password);
    }

    @WebMethod
    public User getRegister(String username, String password) {
        return db.register(username, password);
    }

    @Override
    public List<Package> getAllPackages() {
        return db.allPackages();
    }

    @Override
    public List<RouteEntry> getAllRoutes(int packageId) {
        return db.getAllRoutes(packageId);
    }
}
