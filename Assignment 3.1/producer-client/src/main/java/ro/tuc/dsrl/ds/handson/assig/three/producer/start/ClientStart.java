package ro.tuc.dsrl.ds.handson.assig.three.producer.start;

import ro.tuc.dsrl.ds.handson.assig.three.producer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.queue.communication.DVD;
import ro.tuc.dsrl.ds.handson.assig.three.queue.communication.Message;
import ro.tuc.dsrl.ds.handson.assig.three.queue.communication.MessageContent;

import java.io.IOException;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Producer Client application. This
 *	application will send several messages to be inserted
 *  in the queue server (i.e. to be sent via email by a consumer).
 */
public class ClientStart {
	private static final String HOST = "localhost";
	private static final int PORT = 8888;

	private ClientStart() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection(HOST, PORT);

		try {
			for (int i=0;i<5;i++) {
				//String message = "this is email number " + i;
				MessageContent message = new DVD("Harry Potter and the Chamber of Secrets", 2000 + i, 100.0 * i);
				queue.writeMessage(message);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
