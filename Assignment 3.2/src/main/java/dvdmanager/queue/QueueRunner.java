package dvdmanager.queue;

import dvdmanager.Application;
import dvdmanager.entities.DVDEntity;
import dvdmanager.message.DVD;
import dvdmanager.message.MessageContent;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class QueueRunner {

    private static RabbitTemplate rabbitTemplate;

    public QueueRunner(RabbitTemplate rabbitTemplate) {
        QueueRunner.rabbitTemplate = rabbitTemplate;
    }

    public static void sendMessage(DVDEntity dvd) {
        MessageContent messageContent = new DVD(dvd.getTitle(), dvd.getYear(), dvd.getPrice());
        rabbitTemplate.convertAndSend(Application.queueName, messageContent);
    }
}
