package flights.app.services;

import com.sun.org.apache.xerces.internal.impl.dv.dtd.StringDatatypeValidator;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;
import flights.app.dto.FlightDTO;
import flights.app.entities.City;
import flights.app.entities.Flight;
import flights.app.errorhandler.ResourceNotFoundException;
import flights.app.repositories.CityRepository;
import flights.app.repositories.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrei on 01/11/2016.
 */
@Service
public class FlightService {

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private CityRepository cityRepository;

    public FlightDTO getFlightById(Integer id) {
        Flight flight = flightRepository.findByFlightNumber(id);
        if (flight == null) {
            throw new ResourceNotFoundException(Flight.class.getSimpleName());
        }
        City departureCity = cityRepository.findById(Integer.parseInt(flight.getDepartureCityId()));
        City arrivalCity = cityRepository.findById(Integer.parseInt(flight.getArrivalCityId()));
        if (arrivalCity == null || departureCity == null) {
            throw new ResourceNotFoundException(Flight.class.getSimpleName());
        }

        FlightDTO dto = new FlightDTO.Builder()
                .flightNumber(flight.getFlightNumber())
                .airplaneType(flight.getAirplaneType())
                .departureCityName(departureCity.getName())
                .departureCityLongitude(departureCity.getLongitude())
                .departureCityLatitude(departureCity.getLatitude())
                .departureTime(flight.getDepartureTime())
                .arrivalCityName(arrivalCity.getName())
                .arrivalCityLongitude(arrivalCity.getLongitude())
                .arrivalCityLatitude(arrivalCity.getLatitude())
                .arrivalTime(flight.getArrivalTime())
                .create();

        return dto;
    }

    public List<FlightDTO> findAll() {
        List<Flight> flights = flightRepository.findAll();
        List<FlightDTO> dtos = new ArrayList();
        for (Flight flight : flights) {
            City departureCity = cityRepository.findById(Integer.parseInt(flight.getDepartureCityId()));
            City arrivalCity = cityRepository.findById(Integer.parseInt(flight.getArrivalCityId()));
            FlightDTO dto = new FlightDTO.Builder()
                    .flightNumber(flight.getFlightNumber())
                    .airplaneType(flight.getAirplaneType())
                    .departureCityName(departureCity.getName())
                    .departureCityLongitude(departureCity.getLongitude())
                    .departureCityLatitude(departureCity.getLatitude())
                    .departureTime(flight.getDepartureTime())
                    .arrivalCityName(arrivalCity.getName())
                    .arrivalCityLongitude(arrivalCity.getLongitude())
                    .arrivalCityLatitude(arrivalCity.getLatitude())
                    .arrivalTime(flight.getArrivalTime())
                    .create();
            dtos.add(dto);
        }
        return dtos;
    }

    public Integer create(FlightDTO dto) {
        Flight flight = new Flight();
        flight.setAirplaneType(dto.getAirplaneType());
        City departureCity = cityRepository.findByName(dto.getDepartureCityName());
        if (departureCity == null) {
            departureCity = new City();
            departureCity.setName(dto.getDepartureCityName());
            departureCity.setLongitude(dto.getDepartureCityLongitude());
            departureCity.setLatitude(dto.getDepartureCityLatitude());
            departureCity = cityRepository.save(departureCity);
        }
        flight.setDepartureCityId(departureCity.getId().toString());
        flight.setDepartureTime(getDateFromExternalAPI(dto.getDepartureCityLongitude(), dto.getDepartureCityLatitude()));
        City arrivalCity = cityRepository.findByName(dto.getArrivalCityName());
        if (arrivalCity == null) {
            arrivalCity = new City();
            arrivalCity.setName(dto.getArrivalCityName());
            arrivalCity.setLongitude(dto.getArrivalCityLongitude());
            arrivalCity.setLatitude(dto.getArrivalCityLatitude());
            arrivalCity = cityRepository.save(arrivalCity);
        }
        flight.setArrivalCityId(arrivalCity.getId().toString());
        flight.setArrivalTime(getDateFromExternalAPI(dto.getArrivalCityLongitude(), dto.getArrivalCityLatitude()));

        flight = flightRepository.save(flight);
        return flight.getFlightNumber();
    }

    public void update(FlightDTO dto) {
        Flight flight = new Flight();
        flight.setFlightNumber(dto.getFlightNumber());
        flight.setAirplaneType(dto.getAirplaneType());
        City departureCity = cityRepository.findByName(dto.getDepartureCityName());
        flight.setDepartureCityId(departureCity.getId().toString());
        flight.setDepartureTime(dto.getDepartureTime());
        City arrivalCity = cityRepository.findByName(dto.getArrivalCityName());
        flight.setArrivalCityId(arrivalCity.getId().toString());
        flight.setArrivalTime(dto.getArrivalTime());

        flightRepository.save(flight);
    }

    public void delete(Integer flightNumber) {
        Flight flight = flightRepository.findByFlightNumber(flightNumber);
        flightRepository.delete(flight.getFlightNumber());
    }

    private Date getDateFromExternalAPI(Double longitude, Double latitude) {
        RestTemplate restTemplate = new RestTemplate();
        String timezoneUrl = "http://new.earthtools.org/timezone-1.1/" + latitude.toString() + "/" + longitude.toString();
        String xmlResponse = restTemplate.getForObject(timezoneUrl, String.class);
        String dateTime = "0";
        DOMParser parser = new DOMParser();
        try {
            parser.parse(new InputSource(new StringReader(xmlResponse)));
            Document document = parser.getDocument();
            dateTime = document.getElementsByTagName("localtime").item(0).getTextContent();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
        long date = 0;
        try {
            date = formatter.parse(dateTime).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date(date);
    }
}
