import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Andrei on 27/11/2016.
 */
public class TaxService extends UnicastRemoteObject implements ITaxService {
    public TaxService() throws RemoteException {
        super();
    }

    @Override
    public double computeTax(double engineSize) throws RemoteException {
        if (engineSize <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }
        int sum = 8;
        if (engineSize > 1601) sum = 18;
        if (engineSize > 2001) sum = 72;
        if (engineSize > 2601) sum = 144;
        if (engineSize > 3001) sum = 290;
        return engineSize / 200.0 * sum;
    }
}
