<%--
  Created by IntelliJ IDEA.
  User: Andrei
  Date: 07/11/2016
  Time: 20:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Login to Flights App</title>
</head>
<body>

<form method="post" action="<c:url value="${j_spring_security_check}" />">
    <table>
        <c:if test="${param.error != null}">
            <p>
                Invalid username or password
            </p>
        </c:if>
        <c:if test="${param.logout != null}">
            <p>
                You have been logged out
            </p>
        </c:if>
        <tr>
            <td>E-mail: </td>
            <td><input id="username" name="username" type="text" placeholder="User@email.com"></td>
        </tr>
        <tr>
            <td>Password: </td>
            <td><input id="password" name="password" type="password" placeholder="Password"></td>
        </tr>
        <tr>
            <td><input value="Login" type="submit"></td>
        </tr>
    </table>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>

</body>
</html>
