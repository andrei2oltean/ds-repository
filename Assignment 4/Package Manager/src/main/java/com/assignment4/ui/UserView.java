package com.assignment4.ui;

import com.assignment4.serviceclients.ClientService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;
import wsjavaclient.Package;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@DesignRoot
public class UserView extends HorizontalLayout {

    private ClientService clientService = new ClientService();

    TextField filter = new TextField();
    Grid packageList = new Grid();
    Button listAllPackages = new Button("List all packages");

    public UserView() {
        configureComponents();
        buildLayout();
    }

    private void configureComponents() {

        filter.setInputPrompt("Filter Packages...");
        filter.addTextChangeListener(e -> refreshPackages(e.getText()));

        BeanItemContainer<Package> container = new BeanItemContainer<>(Package.class,
                clientService.getPackages());

        packageList.setContainerDataSource(container);
        refreshPackages();
    }

    private void buildLayout() {
        HorizontalLayout packageActions = new HorizontalLayout(filter, listAllPackages);
        packageActions.setSpacing(true);
        packageActions.setExpandRatio(filter, 1);

        VerticalLayout packageLayout = new VerticalLayout(packageActions, packageList);
        packageList.setHeight(300, Unit.PIXELS);
        packageLayout.setSpacing(true);
        packageLayout.setMargin(true);
        packageLayout.setExpandRatio(packageList, 1);

        addComponent(new VerticalLayout(packageLayout));
        setSpacing(true);
        setMargin(true);
    }

    void refreshPackages() {
        refreshPackages(filter.getValue());
    }

    private void refreshPackages(String stringFilter) {
        BeanItemContainer<Package> container = new BeanItemContainer<>(
                Package.class,
                filterPackages(clientService.getPackages(), stringFilter));
        packageList.setContainerDataSource(container);
    }

    private List<Package> filterPackages(List<Package> packages, String stringFilter) {
        List<Package> filtered = new ArrayList<>();
        for (Package packageObj : packages) {
            if (packageObj.getName().contains(stringFilter)) {
                filtered.add(packageObj);
            }
        }
        return filtered;
    }
}
