package flights.app.errorhandler;

import flights.app.dto.FlightDTO;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.sql.Date;

/**
 * Created by Andrei on 23/11/2016.
 */
public class FlightValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return FlightDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        FlightDTO flight = (FlightDTO) target;

        if (flight.getFlightNumber() == null && flight.getFlightNumber() < 0) {
            errors.rejectValue("flightNumber", "flight.flightNumber", "Flight number must be non-negative");
        }

        if (flight.getAirplaneType() == null) {
            errors.rejectValue("airplaneType", "flight.airplaneType", "Airplane type must have a value");
        }

        if (flight.getDepartureCityName() == null) {
            errors.rejectValue("departureCity", "flight.departureCity", "Departure city must have a value");
        }

        if (flight.getDepartureCityLatitude() == null && flight.getDepartureCityLatitude() < -180.0
                && flight.getDepartureCityLatitude() < -180.0) {
            errors.rejectValue("departureCityLatitude", "flight.departureCityLatitude",
                    "Departure city latitude must be between -180 and 180");
        }

        if (flight.getDepartureCityLongitude() == null && flight.getDepartureCityLongitude() < -180.0
                && flight.getDepartureCityLongitude() < -180.0) {
            errors.rejectValue("departureCityLongitude", "flight.departureCityLongitude",
                    "Departure city longitude must be between -180 and 180");
        }

        if (flight.getDepartureTime() == null) {
            flight.setDepartureTime(new Date(0));
        }

        if (flight.getArrivalCityName() == null) {
            errors.rejectValue("arrivalCity", "flight.arrivalCity", "Arrival city must have a value");
        }

        if (flight.getArrivalCityLatitude() == null && flight.getArrivalCityLatitude() < -180.0
                && flight.getArrivalCityLatitude() < -180.0) {
            errors.rejectValue("arrivalCityLatitude", "flight.arrivalCityLatitude",
                    "Arrival city latitude must be between -180 and 180");
        }

        if (flight.getArrivalCityLongitude() == null && flight.getArrivalCityLongitude() < -180.0
                && flight.getArrivalCityLongitude() < -180.0) {
            errors.rejectValue("arrivalCityLongitude", "flight.arrivalCityLongitude",
                    "Arrival city longitude must be between -180 and 180");
        }

        if (flight.getArrivalTime() == null) {
            flight.setArrivalTime(new Date(0));
        }
    }
}
