package com.assignment4;

import com.assignment4.entities.RouteEntry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PackageManagerApplicationTests {

    @Test
    public void contextLoads() {
    }

    @Test
    public void useFindAllPackages() {

        //PackageClient.useFindAllPackagesService("random");
    }

    @Test
    public void useSaveRoute() {
        RouteEntry routeEntry = new RouteEntry();
        routeEntry.setCity("Dublin");
        routeEntry.setTime(new Date(2016, 01, 01));
        //PackageClient.useSaveRouteService(routeEntry);
    }

}
