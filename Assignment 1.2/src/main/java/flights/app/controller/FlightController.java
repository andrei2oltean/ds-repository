package flights.app.controller;

import flights.app.dto.FlightDTO;
import flights.app.dto.UserDTO;
import flights.app.services.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Andrei on 02/11/2016.
 */
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/flights")
public class FlightController {

    @Autowired
    private FlightService flightService;

    @RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
    public FlightDTO getFlightByFlightNumber(@PathVariable("id") int id) {
        return flightService.getFlightById(id);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<FlightDTO> getAllFlights() {
        return flightService.findAll();
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public int insertFlight(@RequestBody FlightDTO flightDTO) {
        return flightService.create(flightDTO);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public void updateFlight(@RequestBody FlightDTO flightDTO) {
        flightService.update(flightDTO);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void deleteFlight(@PathVariable("id") int flightNumber) {
        flightService.delete(flightNumber);
    }
}
