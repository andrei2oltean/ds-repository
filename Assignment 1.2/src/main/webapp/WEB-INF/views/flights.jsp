<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Andrei
  Date: 08/11/2016
  Time: 23:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Flights App</title>
</head>
<body>
<security:authorize access="hasRole('ROLE_ADMIN')">
    <p>Welcome to the administrator flights page</p>
</security:authorize>
<security:authorize access="hasRole('ROLE_USER')">
    <p>Welcome to the flights app :D</p>
</security:authorize>

<a href="<c:url value="/logout" />">Logout</a>

</body>
</html>
