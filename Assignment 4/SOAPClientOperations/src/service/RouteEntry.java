package service;


import java.util.Date;

public class RouteEntry {

    private int id;
    private String city;
    private Date time;

    public RouteEntry() {
    }

    public RouteEntry(int id, String city, Date time) {
        this.id = id;
        this.city = city;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}