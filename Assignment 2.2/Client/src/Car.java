import java.io.Serializable;

/**
 * Created by Andrei on 02/12/2016.
 */
public class Car implements Serializable {

    private static final long serialVersionUID = 1L;
    private int year;
    private double engineCapacity;
    private double purchasingPrice;

    public Car() {
    }

    public Car(int year, double engineCapacity, double purchasingPrice) {
        this.year = year;
        this.engineCapacity = engineCapacity;
        this.purchasingPrice = purchasingPrice;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(double engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public double getPurchasingPrice() {
        return purchasingPrice;
    }

    public void setPurchasingPrice(double purchasingPrice) {
        this.purchasingPrice = purchasingPrice;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = year;
        temp = Double.doubleToLongBits(engineCapacity);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(purchasingPrice);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Car other = (Car) obj;
        if (engineCapacity != other.engineCapacity)
            return false;
        if (year != other.year)
            return false;
        if (purchasingPrice != other.purchasingPrice)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Car{" +
                "year=" + year +
                ", engineCapacity=" + engineCapacity +
                ", purchasingPrice=" + purchasingPrice +
                '}';
    }
}
