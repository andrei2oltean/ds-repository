<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Andrei
  Date: 20/11/2016
  Time: 19:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User page</title>
</head>
<body>
<p>Welcome to the user page</p>
<table>
    <tr>
        <th>Flight Number</th>
        <th>Airplane Type</th>
        <th>Departure city</th>
        <th>Departure city longitude</th>
        <th>Departure city latitude</th>
        <th>Departure time</th>
        <th>Arrival city</th>
        <th>Arrival city longitude</th>
        <th>Arrival city latitude</th>
        <th>Arrival time</th>
    </tr>
    <c:forEach items="${flights}" var="flight">
        <tr>
            <td>${flight.flightNumber}</td>
            <td>${flight.airplaneType}</td>
            <td>${flight.departureCityName}</td>
            <td>${flight.departureCityLongitude}</td>
            <td>${flight.departureCityLatitude}</td>
            <td>${flight.departureTime}</td>
            <td>${flight.arrivalCityName}</td>
            <td>${flight.arrivalCityLongitude}</td>
            <td>${flight.arrivalCityLatitude}</td>
            <td>${flight.arrivalTime}</td>
        </tr>
    </c:forEach>
</table>
<form method="post" action="<c:url value="/logout" />">
    <input type="submit" value="Logout"/>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>
</body>
</html>
