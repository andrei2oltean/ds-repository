import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by Andrei on 27/11/2016.
 */
public class Client {

    public static double computeTax(final double engineSize) throws RemoteException, NotBoundException {
        Registry myRegistry = LocateRegistry.getRegistry("127.0.0.1", 1097);
        ITaxService taxService = (ITaxService) myRegistry.lookup("computeTax");
        return taxService.computeTax(engineSize);
    }

    public static double computeSellingPrice(final double purchasingPrice, final int year) throws RemoteException, NotBoundException {
        Registry myRegistry = LocateRegistry.getRegistry("127.0.0.1", 1097);
        IPriceService priceService = (IPriceService) myRegistry.lookup("computeSellingPrice");
        return priceService.computeSellingPrice(purchasingPrice, year);
    }
}
