package com.assignment4.loginservice;

import com.assignment4.entities.App_user;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

//Service Endpoint Interface
@WebService
@SOAPBinding(style = Style.RPC)
public interface ClientService {
	
	@WebMethod App_user getLogIn();
	@WebMethod App_user getRegister();
	
}