package com.assignment4.ui;

import com.assignment4.entities.RouteEntry;
import com.assignment4.serviceclients.AdminService;
import com.assignment4.serviceclients.ClientService;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.*;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.ValoTheme;

/* Create custom UI Components.
 *
 * Create your own Vaadin components by inheritance and composition.
 * This is a form component inherited from VerticalLayout. Use
 * Use BeanFieldGroup to bind data fields from DTO to UI fields.
 * Similarly named field by naming convention or customized
 * with @PropertyId annotation.
 */
public class RouteForm extends FormLayout {

    private AdminService adminService = AdminService.getService("java");

    Button save = new Button("Save", this::save);
    Button cancel = new Button("Cancel", this::cancel);
    TextField city = new TextField("City");
    DateField time = new DateField("Time");

    AdminView view;
    RouteEntry selectedRouteEntry;

    BeanFieldGroup<RouteEntry> formFieldBindings;

    public RouteForm(AdminView view) {
        this.view = view;
        configureComponents();
        buildLayout();
    }

    private void configureComponents() {
        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        setVisible(false);
    }

    private void buildLayout() {
        setSizeUndefined();
        setMargin(true);

        HorizontalLayout actions = new HorizontalLayout(save, cancel);
        actions.setSpacing(true);

        addComponents(actions, city, time);
    }

    public void save(Button.ClickEvent event) {
        try {
            formFieldBindings.commit();

            adminService.addRoute(selectedRouteEntry.getCity(),
                    selectedRouteEntry.getTime(),
                    view.getSelectedPackageId());

            String msg = String.format("Saved '%s %s'.", selectedRouteEntry.getCity(), selectedRouteEntry.getTime());
            Notification.show(msg, Type.TRAY_NOTIFICATION);
            view.refreshRoutes();

        } catch (FieldGroup.CommitException e) {
            // Validation exceptions could be shown here
        }
    }

    public void cancel(Button.ClickEvent event) {
        // Place to call business logic.
        Notification.show("Cancelled", Type.TRAY_NOTIFICATION);
        view.routesList.select(null);
    }

    void edit(RouteEntry RouteEntry) {
        this.selectedRouteEntry = RouteEntry;
        if (RouteEntry != null) {
            // Bind the properties of the selectedRouteEntry POJO to fiels in this form
            formFieldBindings = BeanFieldGroup.bindFieldsBuffered(RouteEntry, this);
            city.focus();
        }
        setVisible(RouteEntry != null);
    }

}