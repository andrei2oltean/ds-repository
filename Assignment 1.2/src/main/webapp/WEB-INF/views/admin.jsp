<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Andrei
  Date: 20/11/2016
  Time: 19:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Administrator page</title>
</head>
<body>
<p>Welcome to the administrator page</p>
<table>
    <tr>
        <th>Flight Number</th>
        <th>Airplane Type</th>
        <th>Departure city</th>
        <th>Departure city longitude</th>
        <th>Departure city latitude</th>
        <th>Departure time</th>
        <th>Arrival city</th>
        <th>Arrival city longitude</th>
        <th>Arrival city latitude</th>
        <th>Arrival time</th>
    </tr>
    <c:forEach items="${flights}" var="flight">
        <tr>
            <td>${flight.flightNumber}</td>
            <td>${flight.airplaneType}</td>
            <td>${flight.departureCityName}</td>
            <td>${flight.departureCityLongitude}</td>
            <td>${flight.departureCityLatitude}</td>
            <td>${flight.departureTime}</td>
            <td>${flight.arrivalCityName}</td>
            <td>${flight.arrivalCityLongitude}</td>
            <td>${flight.arrivalCityLatitude}</td>
            <td>${flight.arrivalTime}</td>
        </tr>
    </c:forEach>
</table>
<form method="get" action="<c:url value="/admin/crud" />">
    <table>
        <tr>
            <td>Flight Number:</td>
            <td><input type="number" name="flightNumber" id="flightNumber" placeholder="46273823"></td>
        </tr>
        <tr>
            <td>Airplane Type:</td>
            <td><input type="text" name="airplaneType" id="airplaneType" placeholder="Airbus"></td>
        </tr>
        <tr>
            <td>Departure City Name:</td>
            <td><input type="text" name="departureCityName" id="departureCityName" placeholder="London"></td>
        </tr>
        <tr>
            <td>Departure City Longitude:</td>
            <td><input type="number" name="departureCityLongitude" id="departureCityLongitude" placeholder="-0.11"></td>
        </tr>
        <tr>
            <td>Departure City Latitude:</td>
            <td><input type="number" name="departureCityLatitude" id="departureCityLatitude" placeholder="51.509"></td>
        </tr>
        <tr>
            <td>Departure Time:</td>
            <td><input type="date" name="departureTime" id="departureTime"></td>
        </tr>
        <tr>
            <td>Arrival City Name:</td>
            <td><input type="text" name="arrivalCityName" id="arrivalCityName" placeholder="New York"></td>
        </tr>
        <tr>
            <td>Arrival City Longitude:</td>
            <td><input type="number" name="arrivalCityLongitude" id="arrivalCityLongitude" placeholder="-73.93"></td>
        </tr>
        <tr>
            <td>Arrival City Latitude:</td>
            <td><input type="number" name="arrivalCityLatitude" id="arrivalCityLatitude" placeholder="40.73"></td>
        </tr>
        <tr>
            <td>Arrival Time:</td>
            <td><input type="date" name="arrivalTime" id="arrivalTime"></td>
        </tr>
        <tr>
            <td><input type="submit" value="Insert flight" name="action"/></td>
            <td><input type="submit" value="Update flight" name="action"/></td>
            <td><input type="submit" value="Delete flight" name="action"/></td>
        </tr>
    </table>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>
<form method="post" action="<c:url value="/logout" />">
    <input type="submit" value="Logout"/>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>
</body>
</html>