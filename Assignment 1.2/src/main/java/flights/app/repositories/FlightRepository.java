package flights.app.repositories;

import flights.app.entities.Flight;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Andrei on 01/11/2016.
 */
public interface FlightRepository extends JpaRepository<Flight, Integer> {

    Flight findByFlightNumber(Integer flightNumber);
}
