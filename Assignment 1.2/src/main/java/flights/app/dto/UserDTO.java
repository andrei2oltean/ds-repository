package flights.app.dto;

public class UserDTO {

	private Integer id;
	private String username;
	private String email;
	private String password;

	public UserDTO() {
	}

	public UserDTO(Integer id, String username, String email, String password) {
		super();
		this.id = id;
		this.username = username;
		this.email = email;
		this.password = password;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static class Builder {
		private Integer nestedid;
		private String nestedusername;
		private String nestedemail;
		private String nestedpassword;

		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}

		public Builder username(String username) {
			this.nestedusername = username;
			return this;
		}

		public Builder email(String email) {
			this.nestedemail = email;
			return this;
		}

		public Builder password(String password) {
			this.nestedpassword = password;
			return this;
		}

		public UserDTO create() {
			return new UserDTO(nestedid, nestedusername, nestedemail, nestedpassword);
		}

	}

}
