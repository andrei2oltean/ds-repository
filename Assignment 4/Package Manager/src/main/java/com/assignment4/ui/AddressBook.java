/*
package com.assignment4.ui;

import com.assignment4.entities.Package;
import com.assignment4.adminservice.PackageService;
import com.vaadin.annotations.DesignRoot;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;

import javax.servlet.annotation.WebServlet;

*/
/* User Interface written in Java.
 *
 * Define the user interface shown on the Vaadin generated web page by extending the UI class.
 * By default, a new UI instance is automatically created when the page is loaded. To reuse
 * the same instance, add @PreserveOnRefresh.
 *//*

@SpringUI
@Theme("valo")
@DesignRoot
public class AddressBook extends UI {

    TextField filter = new TextField();
    Grid packageList = new Grid();
    Button newPackage = new Button("New selectedPackage");
    PackageForm PackageForm = new PackageForm();

    // PackageService is a in-memory mock DAO that mimics
    // a real-world datasource. Typically implemented for
    // example as EJB or Spring Data based service.
    PackageService service = PackageService.createDemoService();

    @Override
    protected void init(VaadinRequest request) {
        configureComponents();
        buildLayout();
    }

    private void configureComponents() {
        newPackage.addClickListener(e -> PackageForm.edit(new selectedPackage()));

        filter.setInputPrompt("Filter Packages...");
        filter.addTextChangeListener(e -> refreshPackages(e.getText()));

        packageList.setContainerDataSource(new BeanItemContainer<>(selectedPackage.class));
        packageList.setColumnOrder("firstName", "lastName", "email");
        packageList.removeColumn("id");
        packageList.removeColumn("birthDate");
        packageList.removeColumn("phone");
        packageList.setSelectionMode(Grid.SelectionMode.SINGLE);
        packageList.addSelectionListener(e -> PackageForm.edit((selectedPackage) packageList.getSelectedRow()));
        refreshPackages();
    }

    */
/*
     * Robust layouts.
     *
     * Layouts are components that contain other components. HorizontalLayout
     * contains TextField and Button. It is wrapped with a Grid into
     * VerticalLayout for the left side of the screen. Allow user to resize the
     * components with a SplitPanel.
     *
     * In addition to programmatically building layout in Java, you may also
     * choose to setup layout declaratively with Vaadin Designer, CSS and HTML.
     *//*

    private void buildLayout() {
        HorizontalLayout actions = new HorizontalLayout(filter, newPackage);
        actions.setWidth("100%");
        filter.setWidth("100%");
        actions.setExpandRatio(filter, 1);

        VerticalLayout left = new VerticalLayout(actions, packageList);
        left.setSizeFull();
        packageList.setSizeFull();
        left.setExpandRatio(packageList, 1);

        HorizontalLayout mainLayout = new HorizontalLayout(left, PackageForm);
        mainLayout.setSizeFull();
        mainLayout.setExpandRatio(left, 1);

        // Split and allow resizing
        setContent(mainLayout);
    }

    */
/*
     * Choose the design patterns you like.
     *
     * It is good practice to have separate data access methods that handle the
     * back-end access and/or the user interface updates. You can further split
     * your code into classes to easier maintenance. With Vaadin you can follow
     * MVC, MVP or any other design pattern you choose.
     *//*

    void refreshPackages() {
        refreshPackages(filter.getValue());
    }

    private void refreshPackages(String stringFilter) {
        BeanItemContainer<selectedPackage> dataSource = new BeanItemContainer<>(selectedPackage.class);
        dataSource.addAll(service.findAll(stringFilter));
        packageList.setContainerDataSource(dataSource);
        PackageForm.setVisible(false);
    }

    */
/*
     * Deployed as a Servlet or Portlet.
     *
     * You can specify additional servlet parameters like the URI and UI class
     * name and turn on production mode when you have finished developing the
     * application.
     *//*

    @WebServlet(urlPatterns = "*//*
*/
/*")
    @VaadinServletConfiguration(ui = AddressBook.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }

}
*/
