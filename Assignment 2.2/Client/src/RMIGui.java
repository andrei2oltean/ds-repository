import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Created by Andrei on 02/12/2016.
 */
public class RMIGui {
    private JButton computeTaxButton;
    private JTextArea resultsArea;
    private JButton computeSellingPriceButton;
    private JTextField engineSizeField;
    private JTextField priceField;
    private JTextField fabricationYearField;
    private JPanel mainPanel;

    public RMIGui() {
        computeSellingPriceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Integer fabricationYear = Integer.parseInt(fabricationYearField.getText());
                Double engineSize = Double.parseDouble(engineSizeField.getText());
                Double purchasingPrice = Double.parseDouble(priceField.getText());
                Car myCar = new Car(fabricationYear, engineSize, purchasingPrice);
                String computedPriceText;
                try {
                    computedPriceText = "Selling price: " + Client.computeSellingPrice(myCar.getPurchasingPrice(), myCar.getYear());
                } catch (RemoteException | NotBoundException | IllegalArgumentException e1) {
                    computedPriceText = e1.getMessage();
                }
                resultsArea.setText(computedPriceText);
            }
        });
        computeTaxButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Integer fabricationYear = Integer.parseInt(fabricationYearField.getText());
                Double engineSize = Double.parseDouble(engineSizeField.getText());
                Double purchasingPrice = Double.parseDouble(priceField.getText());
                Car myCar = new Car(fabricationYear, engineSize, purchasingPrice);
                String computedTaxText = null;
                try {
                    computedTaxText = "Tax: " + Client.computeTax(myCar.getEngineCapacity());
                } catch (RemoteException | NotBoundException | IllegalArgumentException e1) {
                    computedTaxText = e1.getMessage();
                }
                resultsArea.setText(computedTaxText);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("RMIGui");
        frame.setMinimumSize(new Dimension(700, 500));
        frame.setContentPane(new RMIGui().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
