package dvdmanager.receivers;

import dvdmanager.message.MessageContent;
import dvdmanager.service.MailService;
import org.springframework.stereotype.Component;

@Component
public class MailSender {

    private static MailService mailService = new MailService("samSmithTestMail@gmail.com", "samsmithabc125");

    protected static void notifySubscribers(MessageContent message) {
        mailService.sendMail("andrei2oltean@gmail.com", "Message from queue", message.toString());
    }
}
