import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Andrei on 02/12/2016.
 */
public interface ITaxService extends Remote {
    double computeTax(final double engineSize) throws RemoteException;
}
