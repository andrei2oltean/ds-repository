package ro.tuc.dsrl.ds.handson.assig.three.queue.communication;

/**
 * Created by Andrei on 07/12/2016.
 */
public class StringContent extends MessageContent {

    private String content;

    public StringContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "StringContent{" +
                "content='" + content + '\'' +
                "} " + super.toString();
    }
}
